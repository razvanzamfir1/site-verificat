from PIL import Image
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Category(models.Model):
    name = models.CharField(max_length=50)
    parent_category = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.name}'


class ServiceProvider(models.Model):
    PERSOANA_FIZICA = 'PF'
    COMPANIE = 'COM'
    TYPE_CHOICES = [
        (PERSOANA_FIZICA, 'Persoana_Fizica'),
        (COMPANIE, 'Companie')

    ]
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=50)
    type = models.CharField(max_length=50, choices=TYPE_CHOICES, default=PERSOANA_FIZICA, )
    phone_nr = models.CharField(max_length=20)
    email = models.EmailField(default="nimic@chiarnimic.wow")
    description = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='static/product_images/', default='static/not_available.png')
    availability = models.BooleanField(default=True)
    latitude = models.FloatField(default=44.43282996363418)
    longitude = models.FloatField(default=26.097626053735162)

    # favourites = models.ManyToManyField(User, related_name='favourite', default=None, blank=True)

    # def __init__(self, *args, **kwargs):
    #     super().__init__(args, kwargs)
    #     self.fovourites = None

    def __str__(self):
        return f'{self.name}...'


class AboutUs(models.Model):
    name = models.CharField(max_length=50)
    position = models.CharField(max_length=250)
    personal_info = models.TextField()
    email = models.EmailField(default="nimic@chiarnimic.wow")

    def __str__(self):
        return f'{self.name}'


class Beneficiary(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    phone_nr = models.CharField(max_length=20, blank=True)
    email = models.EmailField(default="nimic@chiarnimic.wow")
    application = models.TextField()
    image = models.ImageField(upload_to='static/product_images/', default='static/not_available.png')

    def __str__(self):
        return f'{self.name}'

# class BeneficiaryProfile(models.Model):
#     user = models.OneToOneField(Beneficiary, on_delete=models.CASCADE)
#     locality= models.CharField(max_length=20, blank=True)
#     judet=models.CharField(max_length=50, blank=True)


class Ad(models.Model):
    title = models.CharField(max_length=30)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    service_provider = models.ForeignKey(ServiceProvider, on_delete=models.CASCADE)
    type = models.CharField(max_length=50)
    image = models.ImageField(upload_to='static/product_images/', default='static/not_available.png')
    text_ad = models.TextField()
    additional_details = models.TextField()
    price = models.IntegerField()
    contact_person = models.CharField(max_length=30)
    phone_nr = models.CharField(max_length=20)
    email = models.EmailField(default="nimic@chiarnimic.wow")
    locality = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.title}'

    def get_rating(self):
        total = sum(int(review['stars']) for review in self.reviews.values())
        if self.reviews.count() == 0:
            return 0

        return total / self.reviews.count()


class BeneficiaryRequest(models.Model):
    title = models.CharField(max_length=30)
    beneficiary = models.ForeignKey(Beneficiary, on_delete=models.CASCADE, null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='static/product_images/', default='static/not_available.png')
    text = models.TextField()
    phone_nr = models.CharField(max_length=20)
    email = models.EmailField(default="nimic@chiarnimic.wow")
    locality = models.CharField(max_length=50)

    def __str__(self):
        return f'{self.title}'


class Message(models.Model):
    title = models.CharField(max_length=20)
    image = models.ImageField(upload_to='static/product_images/', default='static/not_available.png')
    # user_name = models.CharField(max_length=20)
    message = models.TextField(max_length=100)
    date = models.DateField()
    # user = models.OneToOneField(User, on_delete=models.CASCADE)
    user_from = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_from')
    user_to = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_to')

    def __str__(self):
        return f'{self.title} {self.user_from} {self.user_to}'


class Favorite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    service_provider = models.ForeignKey(ServiceProvider, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.ad}'


class ReviewRating(models.Model):
    service_provider = models.ForeignKey(ServiceProvider, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    subject = models.CharField(max_length=100, blank=True)
    review = models.CharField(max_length=500, blank=True)
    rating = models.FloatField()
    ip = models.CharField(max_length=20, blank=True)
    status = models.BooleanField(default=True)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.subject

class AdReviews(models.Model):
    ad = models.ForeignKey(Ad, related_name='reviews', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField(blank=True, null=True)
    stars = models.IntegerField()
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.stars} {self.user} {self.ad}'



class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    # service_providers = models.OneToOneField(ServiceProvider)
    # beneficiarys = models.OneToOneField(Beneficiary)
    # type = models.CharField(max_length=50)
    #
    image = models.ImageField(default='not_available_png', upload_to='profile_images')
    email = models.EmailField(default="nimic@chiarnimic.wow")
    phone = models.CharField(max_length=20)
    locality = models.CharField(max_length=50)

    # def __str__(self):
    #     return f'{self.user.username} Profile'  # show how we want it to be displayed
    # @receiver(post_save, sender=User)
    # def create_user_profile(sender, instance, created, **kwargs):
    #     if created:
    #         Profile.objects.create(user=instance)
    #
    # @receiver(post_save, sender=User)
    # def save_user_profile(sender, instance, **kwargs):
    #     instance.profile.save()

    # ServiceProvider = models.ManyToManyField(ServiceProvider)

    # @receiver(post_save, sender=User)  # add this
    # def create_user_profile(sender, instance, created, **kwargs):
    #     if created:
    #         UserProfile.objects.create(user=instance)
    #
    # @receiver(post_save, sender=User)  # add this
    # def save_user_profile(sender, instance, **kwargs):
    #     instance.profile.save()

    # Override the save method of the model

    # def save(self):
    #     super().save()
    #
    #     img = Image.open(self.image.path)  # Open image
    #
    #     # resize image
    #     if img.height > 300 or img.width > 300:
    #         output_size = (300, 300)
    #         img.thumbnail(output_size)  # Resize image
    #         img.save(self.image.path)  # Save it again and override the larger image
