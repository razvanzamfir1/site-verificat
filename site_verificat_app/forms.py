from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

from site_verificat_app.models import *


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'


class ServiceProviderForm(forms.ModelForm):
    # name = forms.CharField(max_length=100,
    #                              required=True,
    #                              widget=forms.TextInput(attrs={'placeholder': 'Name',
    #                                                            'class': 'form-control',
    #                                                            }))
    # type = forms.CharField(max_length=100,
    #                             required=True,
    #                             widget=forms.TextInput(attrs={'placeholder': 'Type',
    #                                                           'class': 'form-control',
    #                                                           }))
    # phone_nr = forms.CharField(max_length=100,
    #                            required=True,
    #                            widget=forms.TextInput(attrs={'placeholder': 'Your phone',
    #                                                          'class': 'form-control',
    #                                                          }))
    # email = forms.EmailField(required=True,
    #                          widget=forms.TextInput(attrs={'placeholder': 'Your email',
    #                                                        'class': 'form-control',
    #                                                        }))
    # description = forms.CharField(max_length=50,
    #                             required=True,
    #                             widget=forms.TextInput(attrs={'placeholder': 'Your description',
    #                                                               'class': 'form-control',
    #                                                               }))
    # category = forms.CharField(max_length=50,
    #                             required=True,
    #                             widget=forms.TextInput(attrs={'placeholder': 'Category',
    #                                                               'class': 'form-control',
    #                                                               }))
    # # images = forms.ImageField(max_length=50,
    # #                            required=True,
    # #                            widget=forms.TextInput(attrs={'placeholder': 'Images',
    # #                                                          'class': 'form-control',
    # #                                                          }))
    # availability = forms.BooleanField(
    #                            required=True,
    #                            widget=forms.TextInput(attrs={'placeholder': 'Category',
    #                                                          'class': 'form-control',
    #                                                          }))
    #
    #

    class Meta:
        model = ServiceProvider
        fields = '__all__'
        # exclude = ['user']
        # fields = ['name', 'type', 'phone_nr', 'email', 'description', 'category', 'availability']
        exclude = ['user', 'latitude', 'longitude']


# class Meta:
#     model = ServiceProvider
#     fields = '__all__'
#     exclude = ['user']


class BeneficiaryForm(forms.ModelForm):
    class Meta:
        model = Beneficiary
        fields = '__all__'
        exclude = ['user']


# class RegisterForm(UserCreationForm):
#     # fields we want to include and customize in our form
#     first_name = forms.CharField(max_length=100,
#                                  required=True,
#                                  widget=forms.TextInput(attrs={'placeholder': 'First Name',
#                                                                'class': 'form-control',
#                                                                }))
#     last_name = forms.CharField(max_length=100,
#                                 required=True,
#                                 widget=forms.TextInput(attrs={'placeholder': 'Last Name',
#                                                               'class': 'form-control',
#                                                               }))
#     username = forms.CharField(max_length=100,
#                                required=True,
#                                widget=forms.TextInput(attrs={'placeholder': 'Username',
#                                                              'class': 'form-control',
#                                                              }))
#     email = forms.EmailField(required=True,
#                              widget=forms.TextInput(attrs={'placeholder': 'Email',
#                                                            'class': 'form-control',
#                                                            }))
#     password1 = forms.CharField(max_length=50,
#                                 required=True,
#                                 widget=forms.PasswordInput(attrs={'placeholder': 'Password',
#                                                                   'class': 'form-control',
#                                                                   'data-toggle': 'password',
#                                                                   'id': 'password',
#                                                                   }))
#     password2 = forms.CharField(max_length=50,
#                                 required=True,
#                                 widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password',
#                                                                   'class': 'form-control',
#                                                                   'data-toggle': 'password',
#                                                                   'id': 'password',
#                                                                   }))
#
#     class Meta:
#         model = User
#         fields = ['first_name', 'last_name', 'username', 'email', 'password1', 'password2']
#

# class LoginForm(AuthenticationForm):
#     username = forms.CharField(max_length=100,
#                                required=True,
#                                widget=forms.TextInput(attrs={'placeholder': 'Username',
#                                                              'class': 'form-control',
#                                                              }))
#     password = forms.CharField(max_length=50,
#                                required=True,
#                                widget=forms.PasswordInput(attrs={'placeholder': 'Password',
#                                                                  'class': 'form-control',
#                                                                  'data-toggle': 'password',
#                                                                  'id': 'password',
#                                                                  'name': 'password',
#                                                                  }))
#     remember_me = forms.BooleanField(required=False)
#
#     class Meta:
#         model = User
#         fields = ['username', 'password', 'remember_me']


class AdvacedSearchingForm(forms.Form):
    # toate field-urile cu forms.CharField(ex)
    category = forms.CharField(max_length=30)
    # subcategory = forms.CharField(max_length=50)
    type = forms.CharField(max_length=20)
    zone = forms.CharField(max_length=50)
    rating = forms.IntegerField()


class AdForm(forms.ModelForm):
    class Meta:
        model = Ad
        fields = '__all__'
        # exclude = ['service_provider']
        widgets = {'service_provider': forms.HiddenInput(attrs={})}


class BeneficiaryRequestForm(forms.ModelForm):
    class Meta:
        model = BeneficiaryRequest
        fields = '__all__'
        # exclude = ['beneficiary']


class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = '__all__'
        # exclude = ['service_provider']


class ReviewForm(forms.ModelForm):
    class Meta:
        model = ReviewRating
        fields = ['subject', 'review', 'rating']


# Create a UserUpdateForm to update username and email
class UserForm(forms.ModelForm):
    class Meta:
        model = Beneficiary
        fields = ('name', 'phone_nr', 'email', 'image')

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'


# Create a ProfileUpdateForm to update image

# class UserBeneficiaryProfileForm(forms.ModelForm):
#     class Meta:
#         model = BeneficiaryProfile
#         fields = ('locality', 'judet')
#
#     def __init__(self, *args, **kwargs):
#            super(UserBeneficiaryProfileForm, self).__init__(*args, **kwargs)
#            for field in self.fields:
#                self.fields[field].widget.attrs['class'] = 'form-control'
# #
