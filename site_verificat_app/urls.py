from os import path

from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from site_verificat_app import views
# from site_verificat_app.views import profile

urlpatterns = [
    path('', views.home_page_view, name='home'),
    path('about_us/', views.about_us_view, name='about_us'),
    path('admin_page/', views.AdminPageView.as_view(), name='admin_page'),
    # path('ad_admin_page/', views.AdminAdPageView.as_view(), name='ad_admin_page'),
    # path('beneficiary_admin_page/', views.AdminBeneficiaryPageView.as_view(), name='ad_admin_page'),
    path('category_details/<int:category_id>/', views.category_details_view, name='category_details'),
    # path('service_provider_detail/<int:pk>/', views.ServiceProviderDetailView.as_view(), name='service_provider_details'),
    path('service_provider_details/<int:id>/', views.service_provider_details, name='service_provider_details'),
    path('ad_details/<int:pk>/', views.AdDetailView.as_view(), name='ad_details'),
    path('message_details/<int:pk>/', views.MessageDetailView.as_view(), name='message_details'),
    path('beneficiary_details/<int:pk>/', views.BeneficiaryDetailView.as_view(), name='beneficiary_details'),
    path('beneficiary_request_details/<int:pk>/', views.BeneficiaryRequestDetailView.as_view(), name='beneficiary_request_details'),
    path('category_all_view/', views.category_all_view, name='category_all_view'),
    path('service_provider_all_view/', views.service_provider_all_view, name='service_provider_all_view'),
    path('message_all_view/', views.message_all_view, name='message_all_view'),
    path('category_create/', views.CategoryCreateView.as_view(), name='category_create'),
    path('category_update/<int:pk>/', views.CategoryUpdateView.as_view(), name='category_update'),
    path('category_delete/<int:pk>/', views.CategoryDeleteView.as_view(), name='category_delete'),
    path('service_provider_create/', views.ServiceProviderCreateView.as_view(), name='service_provider_create'),
    path('service_provider_update/<int:pk>/', views.ServiceProviderUpdateView.as_view(), name='service_provider_update'),
    path('service_provider_delete/<int:pk>/', views.ServiceProviderDeleteView.as_view(), name='service_provider_delete'),
    path('register_beneficiary/', views.register_beneficiary_view, name='register_beneficiary'),
    path('register_service_provider/', views.register_service_provider_view, name='register_service_provider'),
    path('profile/', views.edit_profile, name='profile'),
    path('search_results/', views.search_view, name='search_results'),
    path('advanced_searching/', views.advanced_searching_view, name='advanced_searching'),
    path('ad_all_view/', views.ad_all_view, name='ad_all_view'),
    # path('rating_all_view/', views.rating_all_view, name='rating_all_view'),
    path('beneficiary_all_view/', views.beneficiary_all_view, name='beneficiary_all_view'),
    path('beneficiary_request_all_view/', views.beneficiary_request_all_view, name='beneficiary_request_all_view'),
    path('ad_create/', views.AdCreateView.as_view(), name='ad_create'),
    path('message_create/', views.MessageCreateView.as_view(), name='message_create'),
    # path('rating_create/', views.RatingCreateView.as_view(), name='rating_create'),
    path('beneficiary_create/', views.BeneficiaryCreateView.as_view(), name='beneficiary_create'),
    path('beneficiary_request_create/', views.BeneficiaryRequestCreateView.as_view(), name='beneficiary_request_create'),
    path('ad_update/<int:pk>/', views.AdUpdateView.as_view(), name='ad_update'),
    path('message_update/<int:pk>/', views.MessageUpdateView.as_view(), name='message_update'),
    path('beneficiary_update/<int:pk>/', views.BeneficiaryUpdateView.as_view(), name='beneficiary_update'),
    path('beneficiary_request_update/<int:pk>/', views.BeneficiaryRequestUpdateView.as_view(), name='beneficiary_request_update'),
    path('ad_delete/<int:pk>/', views.AdDeleteView.as_view(), name='ad_delete'),
    path('message_delete/<int:pk>/', views.MessageDeleteView.as_view(), name='message_delete'),
    path('beneficiary_delete/<int:pk>/', views.BeneficiaryDeleteView.as_view(), name='beneficiary_delete'),
    path('beneficiary_request_delete/<int:pk>/', views.BeneficiaryRequestDeleteView.as_view(), name='beneficiary_request_delete'),
    path('favourite/<int:id>/', views.favourite_add, name='favourite_add'),
    path('favourite_list/', views.favourite_list, name='favourite_list'),
    # path('submit_review/<int:service_provider_id>/', views.submit_review, name='submit_review'),
] + static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)