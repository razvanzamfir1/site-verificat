import geopy
from geopy.distance import geodesic


def coord_and_coord_in_radius(lat1, lon1, lat2, lon2, radius):
    coords_1 = (lat1, lon1)
    coords_2 = (lat2, lon2)

    # distance= geopy.distance.vincenty(coords_1, coords_2).km
    distance = geodesic(coords_1, coords_2).km
    print(distance, radius)
    return distance <= float(radius)
