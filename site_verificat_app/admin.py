from django.contrib import admin
from django.contrib.auth.models import Permission

from site_verificat_app.models import *

# class CommentAdmin(admin.ModelAdmin):
#     list_display = ['subject', 'comment', 'status', 'create_at' ]
#     list_filter = ['status']
#     readonly_fields = ('subject', 'comment', 'ip', 'user', 'service_provider', 'rate')
admin.site.register(Category)
admin.site.register(ServiceProvider)
admin.site.register(Beneficiary)
admin.site.register(AboutUs)
admin.site.register(Ad)
admin.site.register(BeneficiaryRequest)
admin.site.register(Message)
admin.site.register(Favorite)
admin.site.register(ReviewRating)
admin.site.register(Permission)
admin.site.register(Profile)
admin.site.register(AdReviews)
