import json

import geopy
import requests
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import permission_required, login_required
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.mixins import PermissionRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User, Permission
from django.http import HttpResponseForbidden, HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView, DetailView, CreateView, UpdateView, DeleteView

from site_verificat_app import utils
from site_verificat_app.forms import BeneficiaryForm, ServiceProviderForm, CategoryForm, AdForm, BeneficiaryRequestForm, \
    MessageForm, ReviewForm, ReviewForm, UserForm#, UserBeneficiaryProfileForm
from site_verificat_app.models import Category, AboutUs, AboutUs, ServiceProvider, Ad, Beneficiary, BeneficiaryRequest, \
    Message, Favorite, ReviewRating, AdReviews#, BeneficiaryProfile
from users_app.forms import UserUpdateForm


def home_page_view(request):
    all_categories = Category.objects.all
    return render(request, 'pages/homepage.html', {'categories': all_categories})


class StaffRequiredMixin(UserPassesTestMixin):
    def __init__(self):
        self.request = None

    def test_func(self):
        return self.request.user.is_staff


class AdminPageView(TemplateView):
    template_name = 'pages/admin_page.html'


class StaffStatusDecorator:
    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        request = args[0]
        if request.user.is_staff:
            result = self.func(*args, **kwargs)
            return result
        else:
            return HttpResponseForbidden()


@permission_required('site_verificat_app.view_category')
@StaffStatusDecorator
def category_all_view(request):
    all_categories = Category.objects.all()
    return render(request, 'pages/category_all_view.html',
                  {'categories': all_categories})


@permission_required('site_verificat_app.view_service_provider')
@StaffStatusDecorator
def service_provider_all_view(request):
    all_service_providers = ServiceProvider.objects.all()
    return render(request, 'pages/service_provider_all_view.html',
                  {'service_providers': all_service_providers})


# # @permission_required('site_verificat_app.view_beneficiary')
# # @StaffStatusDecorator
# def beneficiary_all_view(request):
#     all_beneficiarys = Beneficiary.objects.all()
#     return render(request, 'pages/beneficiary_all_view.html',
#                   {'beneficiarys': all_beneficiarys})


def category_details_view(request, category_id):
    category = Category.objects.get(id=category_id)  # daca vrem sa scoatem fix un obiect, folosim .get
    all_categories = Category.objects.all()
    service_providers = ServiceProvider.objects.filter(availability=True, category_id=category_id)
    ads = Ad.objects.filter(category_id=category_id)
    beneficiarys = Beneficiary.objects.filter(category_id=category_id)
    # beneficiaries_requests = BeneficiaryRequest.objects.filter(category_id=category_id)
    return render(request, 'pages/category_details.html',
                  {'categories': all_categories, 'category': category, 'service_providers': service_providers,
                   'ads': ads, 'beneficiarys': beneficiarys})


# class ServiceProviderDetailView(DetailView):
#     template_name = 'pages/service_provider_details.html'
#     model = ServiceProvider
#     context_object_name = 'service_provider'

def service_provider_details(request, id):
    # category = Category.objects.all()
    service_provider = ServiceProvider.objects.get(pk=id)
    reviews = ReviewRating.objects.filter(service_provider_id=id)
    context = {'service provider': service_provider,
               # 'category': category,
               'reviews': reviews,
    }
    return render(request, 'pages/service_provider_details.html', context)


def about_us_view(request):
    all_names = AboutUs.objects.all()
    return render(request, 'pages/about_us.html', {'about_us': all_names})


class CategoryCreateView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
    permission_required = 'site_verificat_app.add_category'
    model = Category
    form_class = CategoryForm
    template_name = 'pages/category_create.html'
    success_url = '/admin_page'


class CategoryUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'site_verificat_app.change_category'
    model = Category
    form_class = CategoryForm
    template_name = 'pages/category_update.html'
    success_url = '/admin_page'


class CategoryDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'site_verificat_app.delete_category'
    # primeste un model si incarca un template in care confirmam stergerea respectivului articol
    model = Category
    template_name = 'pages/category_delete.html'
    success_url = '/admin_page'

    def get_context_data(self,
                         **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
        context: dict = super().get_context_data(
            **kwargs)
        category = self.object  # pe proprietatea object isi tine obiectul curent update, delete si detail view
        context['category'] = category  # pasam categoria pe cheia category
        return context


class ServiceProviderCreateView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
    permission_required = 'site_verificat_app.add_service_provider'
    model = ServiceProvider
    form_class = ServiceProviderForm
    template_name = 'pages/service_provider_create.html'
    success_url = '/admin_page'


class ServiceProviderUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'site_verificat_app.change_service_provider'
    model = ServiceProvider
    form_class = ServiceProviderForm
    template_name = 'pages/service_provider_update.html'
    success_url = '/admin_page'


class ServiceProviderDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'site_verificat_app.delete_service_provider'
    # primeste un model si incarca un template in care confirmam stergerea respectivului articol
    model = ServiceProvider
    template_name = 'pages/service_provider_delete.html'
    success_url = '/admin_page'

    def get_context_data(self,
                         **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
        context: dict = super().get_context_data(
            **kwargs)
        service_provider = self.object  # pe proprietatea object isi tine obiectul curent update, delete si detail view
        context['service_provider'] = service_provider  # pasam produsul pe cheia product
        return context

@login_required
def edit_profile(request):
    user_form = UserForm(instance=request.user)
    context = {
        'user_form': user_form
    }
    return render(request, 'pages/profile.html', context)

# @login_required
# def edit_beneficiaryprofile(request):
    # beneficiaryprofile= get_object_or_404(BeneficiaryProfile, user=request.user.beneficiary)
    # if request.method == 'GET':
    #     user_form = UserForm(request.POST, instance=request.user)
        # profile_form = UserBeneficiaryProfileForm(request.POST, instance=request.user.beneficiary)
        # if user_form.is_valid(): #and profile_form.is_valid():
        #     user_form.save()
        #     # profile_form.save()
        #     return redirect('edit_profile')
        # else:
        #     user_form = UserForm(instance=request.user)
            # profile_form = UserBeneficiaryProfileForm(instance=request.user.beneficiary)
        # context = {
        #     'user_form': user_form,
        #     # 'profile_form': profile_form,
        #     # 'userprofile': request.user.beneficiary,
        # }
        # return render(request, 'pages/profile.html', context)

def register_beneficiary_view(request):
    if request.method == 'POST':
        # instantiem cele 2 formulare folosind datele din dictionarul request.POST
        user_form = UserCreationForm(data=request.POST)
        beneficiary_form = BeneficiaryForm(data=request.POST)
        if user_form.is_valid() and beneficiary_form.is_valid():
            user: User = user_form.save()
            # # permission = Permission.objects.get(codename='add_beneficiary_request')
            # user.user_permissions.add(permission)
            # permission = Permission.objects.get(codename='change_beneficiary_request')
            # user.user_permissions.add(permission)
            # permission = Permission.objects.get(codename='delete_beneficiary_request')
            # user.user_permissions.add(permission)
            user.save()
            beneficiary = beneficiary_form.save()
            # salvam profilul de client si facem un update la coloana user si legam de user-ul creat mai sus
            beneficiary.user = user
            beneficiary.save()
            return redirect('/')
    else:  # method=get
        # instantiem 2 formulare goale si le trimitem catre template
        user_form = UserCreationForm()
        beneficiary_form = BeneficiaryForm()
    # Pe get, formularele sunt goale. Pe post, formularele sunt cele instantiate mai sus in if(populate).
    return render(request, 'pages/register_beneficiary.html',
                  {'user_form': user_form, 'beneficiary_form': beneficiary_form})


def register_service_provider_view(request):
    if request.method == 'POST':
        # instantiem cele 2 formulare folosind datele din dictionarul request.POST
        user_form = UserCreationForm(data=request.POST)
        service_provider_form = ServiceProviderForm(data=request.POST)
        if user_form.is_valid() and service_provider_form.is_valid():
            user: User = user_form.save()
            permission = Permission.objects.get(codename='add_ad')
            user.user_permissions.add(permission)
            permission = Permission.objects.get(codename='change_ad')
            user.user_permissions.add(permission)
            permission = Permission.objects.get(codename='delete_ad')
            user.user_permissions.add(permission)
            user.save()
            service_provider = service_provider_form.save()
            # salvam profilul de client si facem un update la coloana user si legam de user-ul creat mai sus
            service_provider.user = user
            service_provider.save()
            return redirect('/')
    else:  # method=get
        # instantiem 2 formulare goale si le trimitem catre template
        user_form = UserCreationForm()
        service_provider_form = ServiceProviderForm()
    # Pe get, formularele sunt goale. Pe post, formularele sunt cele instantiate mai sus in if(populate).
    return render(request, 'pages/register_service_provider.html',
                  {'user_form': user_form, 'service_provider_form': service_provider_form})


def loghin_request(request):
    if request.method == "POST":
        user_form = AuthenticationForm(request, data=request.POST)
        if user_form.is_valid():
            username = user_form.cleaned_data.get("username")
            password = user_form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, {username})
                return redirect('/')
            else:
                messages.error(request, "Invalid username or password")
        user_form = AuthenticationForm()
        return render(request, 'registration/login.html', {'user_form': user_form})


def search_view(request):
    print(request.GET)
    user_search = request.GET.get('user_search', None)
    service_providers2 = ServiceProvider.objects.filter(name__icontains=user_search)
    service_providers3 = ServiceProvider.objects.filter(type__icontains=user_search)
    service_providers4 = ServiceProvider.objects.filter(phone_nr__icontains=user_search)
    service_providers5 = ServiceProvider.objects.filter(email__icontains=user_search)
    service_providers = list(set(service_providers2).union(
        set(service_providers3).union(set(service_providers4).union(set(service_providers5)))))
    ads2 = Ad.objects.filter(title__icontains=user_search)
    ads3 = Ad.objects.filter(type__icontains=user_search)
    ads4 = Ad.objects.filter(phone_nr__icontains=user_search)
    ads5 = Ad.objects.filter(email__icontains=user_search)
    ads6 = Ad.objects.filter(locality__icontains=user_search)
    ads = list(set(ads2).union(set(ads3).union(set(ads4).union(set(ads5).union(set(ads6))))))
    categories = Category.objects.filter(name__icontains=user_search)
    # service_providers = ServiceProvider.objects.filter(phone_nr__contains=user_search)
    # service_providers = ServiceProvider.objects.filter(email__contains=user_search)
    # service_providers = ServiceProvider.objects.filter(description__contains=user_search)
    # ads = Ad.objects.all()
    return render(request, 'pages/search_results.html',
                  {'service_providers': service_providers, 'categories': categories, 'ads': ads})


def advanced_searching_view(request):
    category_id = request.GET.get('category', None)
    zone = request.GET.get('zone', None)
    type = request.GET.get('type', None)
    print(category_id)
    print(zone)
    print(type)
    service_providers = ServiceProvider.objects.all()
    if zone is not None:
        radius = request.GET.get('radius', '0')
        print(request.GET)
        zone_transforms = zone.lower().replace(' ', '+')
        url = f'https://nominatim.openstreetmap.org/search?q={zone_transforms}&format=json&polygon=1&addressdetails=1'
        result = requests.get(url)
        print(result.content)
        coord_dict = json.loads(result.content)
        print(coord_dict)
        first_result = coord_dict[0]
        latitude = first_result['lat']
        longitude = first_result['lon']
        service_providers = list(filter(
            lambda service_provider: utils.coord_and_coord_in_radius(latitude, longitude, service_provider.latitude,
                                                                     service_provider.longitude, radius),
            service_providers))
        print(len(service_providers))

    if category_id is not None:
        service_providers = list(
            filter(lambda service_provider: service_provider.category.id == int(category_id), service_providers))
        print(len(service_providers))

    if type is not None:
        service_providers = list(
            filter(lambda service_provider: service_provider.type == type, service_providers))
        print(len(service_providers))

    # user_search = request.GET.get('user_search', '')
    # category_search = request.GET.get('category_search', '')
    # subcategory_search = request.GET.get('subcategory_search', '')
    # type_search = request.GET.get('type_search', '')

    return render(request, 'pages/advanced_searching.html',
                  {'service_providers': service_providers})


@permission_required('site_verificat_app.view_ad')
def ad_all_view(request):
    all_ads = Ad.objects.all()
    return render(request, 'pages/ad_all_view.html',
                  {'ads': all_ads})


class AdDetailView(DetailView):
    template_name = 'pages/ad_details.html'
    model = Ad
    context_object_name = 'ad'

    def post(self, request, *args, **kwargs):
        print('am ajuns')
        # Add review
        if request.method == 'POST' and request.user.is_authenticated:
            stars = request.POST.get('stars', 3)
            content = request.POST.get('content', '')
            # context = self.get_context_data()
            review = AdReviews.objects.create(ad=self.get_object(), user=request.user, stars=stars, content=content)

            return HttpResponseRedirect(request.META['HTTP_REFERER'])


class AdCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'site_verificat_app.add_ad'
    model = Ad
    form_class = AdForm
    template_name = 'pages/ad_create.html'
    success_url = '/admin_page'

    def form_valid(self, form):
        valid = super().form_valid(form)
        return valid

    def get_initial(self):
        user_id = self.request.user.id
        service_provider = ServiceProvider.objects.get(user_id=user_id)
        return {'service_provider': service_provider.id}


class AdUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'site_verificat_app.change_ad'
    model = Ad
    form_class = AdForm
    template_name = 'pages/ad_update.html'
    success_url = '/admin_page'


class AdDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'site_verificat_app.delete_ad'
    # primeste un model si incarca un template in care confirmam stergerea respectivului articol
    model = Ad
    template_name = 'pages/ad_delete.html'
    success_url = '/admin_page'

    def get_context_data(self,
                         **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
        context: dict = super().get_context_data(
            **kwargs)
        ad = self.object  # pe proprietatea object isi tine obiectul curent update, delete si detail view
        context['ad'] = ad  # pasam produsul pe cheia product
        return context


@permission_required('site_verificat_app.view_beneficiary')
@StaffStatusDecorator
def beneficiary_all_view(request):
    all_beneficiaries = Beneficiary.objects.all()
    return render(request, 'pages/beneficiary_all_view.html',
                  {'beneficiaries': all_beneficiaries})


class BeneficiaryDetailView(DetailView):
    template_name = 'pages/beneficiary_details.html'
    model = Beneficiary
    context_object_name = 'beneficiary'


class BeneficiaryCreateView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
    permission_required = 'site_verificat_app.add_beneficiary'
    model = Beneficiary
    form_class = BeneficiaryForm
    template_name = 'pages/beneficiary_create.html'
    success_url = '/admin_page'


class BeneficiaryUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'site_verificat_app.change_beneficiary'
    model = Beneficiary
    form_class = BeneficiaryForm
    template_name = 'pages/beneficiary_update.html'
    success_url = '/admin_page'


class BeneficiaryDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'site_verificat_app.delete_beneficiary'
    # primeste un model si incarca un template in care confirmam stergerea respectivului articol
    model = Beneficiary
    template_name = 'pages/beneficiary_delete.html'
    success_url = '/admin_page'

    def get_context_data(self,
                         **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
        context: dict = super().get_context_data(
            **kwargs)
        beneficiary = self.object  # pe proprietatea object isi tine obiectul curent update, delete si detail view
        context['beneficiary'] = beneficiary  # pasam produsul pe cheia product
        return context


@permission_required('site_verificat_app.view_beneficiary_request')
def beneficiary_request_all_view(request):
    all_beneficiaries_requests = BeneficiaryRequest.objects.all()
    return render(request, 'pages/beneficiary_request_all_view.html',
                  {'beneficiaries_requests': all_beneficiaries_requests})


class BeneficiaryRequestDetailView(DetailView):
    template_name = 'pages/beneficiary_request_details.html'
    model = BeneficiaryRequest
    context_object_name = 'beneficiary_request'


class BeneficiaryRequestCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'site_verificat_app.add_beneficiary_request'
    model = BeneficiaryRequest
    form_class = BeneficiaryRequestForm
    template_name = 'pages/beneficiary_request_create.html'
    success_url = '/admin_page'

    def form_valid(self, form):
        valid = super().form_valid(form)
        return valid

    def get_initial(self):
        user_id = self.request.user.id
        beneficiary = Beneficiary.objects.get(user_id=user_id)
        return {'beneficiary': beneficiary.id}


class BeneficiaryRequestUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'site_verificat_app.change_beneficiary_request'
    model = BeneficiaryRequest
    form_class = BeneficiaryRequestForm
    template_name = 'pages/beneficiary_request_update.html'
    success_url = '/admin_page'


class BeneficiaryRequestDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'site_verificat_app.delete_beneficiary_request'
    # primeste un model si incarca un template in care confirmam stergerea respectivului articol
    model = BeneficiaryRequest
    template_name = 'pages/beneficiary_request_delete.html'
    success_url = '/admin_page'

    def get_context_data(self,
                         **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
        context: dict = super().get_context_data(
            **kwargs)
        beneficiary_request = self.object  # pe proprietatea object isi tine obiectul curent update, delete si detail view
        context['beneficiary_request'] = beneficiary_request  # pasam produsul pe cheia product
        return context


# class AdminBeneficiaryPageView(TemplateView):
#     template_name = 'pages/beneficiary_admin_page.html'
#
#
# class AdminAdPageView(TemplateView):
#     template_name = 'pages/ad_admin_page.html'

def message_all_view(request):
    all_messages2 = Message.objects.filter(user_from=request.user)
    all_messages3 = Message.objects.filter(user_to=request.user)
    all_messages = list(set(all_messages2).union(set(all_messages3)))
    return render(request, 'pages/message_all_view.html',
                  {'messages': all_messages})


class MessageDetailView(DetailView):
    template_name = 'pages/message_details.html'
    model = Message
    context_object_name = 'message'


class MessageCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'site_verificat_app.add_message'
    model = Message
    form_class = MessageForm
    template_name = 'pages/message_create.html'
    success_url = '/admin_page'

    # def form_valid(self, form):
    #     valid = super().form_valid(form)
    #     return valid
    #
    # def get_initial(self):
    #     user_id = self.request.user.id
    #     service_provider = ServiceProvider.objects.get(user_id=user_id)
    #     return {'service_provider': service_provider.id}


class MessageUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'site_verificat_app.change_message'
    model = Message
    form_class = MessageForm
    template_name = 'pages/message_update.html'
    success_url = '/admin_page'


class MessageDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'site_verificat_app.delete_message'
    # primeste un model si incarca un template in care confirmam stergerea respectivului articol
    model = Message
    template_name = 'pages/message_delete.html'
    success_url = '/admin_page'

    def get_context_data(self,
                         **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
        context: dict = super().get_context_data(
            **kwargs)
        ad = self.object  # pe proprietatea object isi tine obiectul curent update, delete si detail view
        context['ad'] = ad  # pasam produsul pe cheia product
        return context


@login_required
def favourite_list(request):
    favourites = Favorite.objects.filter(user=request.user)
    service_providers = [favourite.service_provider for favourite in favourites]
    ads = Ad.objects.filter(service_provider__in= service_providers)

    # favourites = ServiceProvider.objects.filter(favourites__contains=request.user)
    return render(request, 'pages/favourites.html', {'service_providers': service_providers, 'ads':ads})


@login_required
def favourite_add(request, id):
    existing_favourites = Favorite.objects.filter(service_provider_id=id, user=request.user)
    if existing_favourites.count() > 0:
        existing_favourites.first().delete()
    else:
        Favorite.objects.create(service_provider_id=id, user=request.user)

    return HttpResponseRedirect(request.META['HTTP_REFERER'])

# @login_required
# def ad_list(request):
#     ads = Ad.objects.filter(user=request.user, service_provider=request.service_provider)
#     service_providers = [ad.service_provider for ad in ads]
#     # favourites = ServiceProvider.objects.filter(favourites__contains=request.user)
#     return render(request, 'pages/favourites.html', {'service_providers': service_providers})

# def rating_all_view(request):
#     all_ratings = Comment.objects.all()
#     return render(request, 'pages/rating_all_view.html',
#                   {'ratings': all_ratings})
#
# class RatingCreateView(PermissionRequiredMixin, CreateView):
#     permission_required = 'site_verificat_app.add_rating'
#     model = Comment
#     form_class = CommentForm
#     template_name = 'pages/rating_create.html'
#     success_url = '/'


# def addcomment(request,id):
#     # return HttpResponse("Add comment")
#     url = request.META.get('HTTP_REFERER') # GET LAST URL
#     # return HttpResponse(url)
#     if request.method == 'POST':  # CHECK POST
#         form = CommentForm(request.POST)
#         if form.is_valid():
#             data = ReviewRating()
#             data.subject = form.cleaned_data['subject']
#             data.review = form.cleaned_data['review']
#             data.rating = form.cleaned_data['rating']
#             data.ip = request.META.get('REMOTE_ADDR')
#             data.service_provider_id =id
#             current_user = request.user
#             data.user_id = current_user.id
#             data.save()
#             messages.success(request, "Recenzia ta a fost trimisa. Multumim pentru interesul tau.")
#             return HttpResponseRedirect(url)


# def submit_review(request, service_provider_id):
#     print('am ajuns')
#     url = request.META.get('HTTP_REFERER')
#     if request.method == 'POST':
#         try:
#             reviews = ReviewRating.objects.get(user__id=request.user.id, service_provider__id=service_provider_id)
#             form = ReviewForm(request.POST, instance=reviews)
#             form.save()
#             messages.success(request, 'Recenzia ta a fost modificata. Multumim.')
#             return redirect(url)
#         except ReviewRating.DoesNotExist:
#             form = ReviewForm(request.POST)
#             if form.is_valid():
#                 data = ReviewRating()
#                 data.subject = form.cleaned_data['subject']
#                 data.rating = form.cleaned_data['rating']
#                 data.review = form.cleaned_data['review']
#                 data.ip = request.META.get('REMOTE_ADDR')
#                 data.service_provider_id = service_provider_id
#                 data.user_id = request.user.id
#                 data.save()
#                 messages.success(request, 'Recenzia ta a fost trimisa. Multumim')
#                 return redirect(url)


# @login_required
# def profile(request):
#     if request.method == 'POST':
#         u_form = UserUpdateForm(request.POST, instance=request.user)
#         p_form = ProfileUpdateForm(request.POST,
#                                    request.FILES,
#                                    instance=request.user.profile)
#         if u_form.is_valid() and p_form.is_valid():
#             u_form.save()
#             p_form.save()
#             messages.success(request, f'Your account has been updated!')
#             return redirect('profile') # Redirect back to profile page

    # else:
    #     u_form = UserUpdateForm(instance=request.user)
    #     p_form = ProfileUpdateForm(instance=request.user.profile)
    #
    # context = {
    #     'u_form': u_form,
    #     'p_form': p_form
    # }
    #
    # return render(request, 'users_app/profile.html', context)
