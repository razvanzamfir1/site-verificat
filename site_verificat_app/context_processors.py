from site_verificat_app.models import Category, ServiceProvider, Beneficiary


def navbar_data_provider(request):
    all_categories = Category.objects.all()
    return {'categories': all_categories}

# def favourite_provider(request):
#     all_service_providers = ServiceProvider.objects.all()
#     all_beneficiaries = Beneficiary.objects.all()
#     return {'service_providers': all_service_providers, 'beneficiaries': all_beneficiaries}